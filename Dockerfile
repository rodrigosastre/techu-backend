FROM node

WORKDIR /api-uruguay

ADD . /api-uruguay

RUN npm install

EXPOSE 3001

CMD ["node", "app_mlab.js"]
