var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var config = require('./config/config.js');
var jwtConfig = require('./config/jwtConfig.js');
var users = require('./Controllers/users_mlab.js');
var accounts = require('./Controllers/accounts_mlab.js');
var movements = require('./Controllers/movements.js');

var app = express();
app.use( bodyParser.json() );
app.use(jwtConfig.config.unless({path: [`${config.URI}/login`, `${config.URI}/users`]}));
app.use(cors());

// users
app.post(config.URI + '/login', users.login);
app.post(config.URI + '/logout', users.logout);
app.get(config.URI + '/users', users.getUsers);
app.get(config.URI + '/users/:userId', users.getUserByID);
app.post(config.URI + '/users', users.postUser);
app.put(config.URI + '/users/:userId', users.putUser);
app.delete(config.URI + '/users/:userId', users.deleteUser);

// accounts
app.get(config.URI + '/accounts', accounts.getUserAccounts);

// movements
app.get(config.URI + '/movements', movements.getAccountMovements);

app.listen(config.port);
console.log(`Escuchando en el puerto ${config.port}...`);
