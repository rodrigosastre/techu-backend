var request = require('request-json');
var config = require('../config/config.js');
var jwt = require('jsonwebtoken');

// login
var login = (req, res) => {
  var email = req.body.email;
  var pass = req.body.password;
  var client = request.createClient(config.mlabHost);
  var queryStringEmail = 'q={"email":"' + email + '"}';
  client.get(`${config.baseURL}/users?${queryStringEmail}&apiKey=${config.apiKey}`, (err, resMlab , body) => {
    let retStatus = 500;
    let retBody = {};
    var respuesta = body[0];
    if (respuesta != undefined) {
      if (respuesta.password == pass) {
        var token = jwt.sign({ email: respuesta.email }, config.jwtSecret, { expiresIn: config.tokenExpiration });
        var session = {"logged":true};
        var login = '{"$set":' + JSON.stringify(session) + '}';
        client.put(`${config.baseURL}/users?q={"id": ${respuesta.id}}&apiKey=${config.apiKey}`, JSON.parse(login), (errP, resMlabP, bodyP) => {
          retStatus = 200;
          retBody = { "email": email, "token": token };
          res.status(retStatus).send(retBody);
        });
      }
      else {
        retStatus = 401;
        retBody = { "msg": "Wrong password" };
        res.status(retStatus).send(retBody);
      }
    } else {
      retStatus = 401;
      retBody = { "msg": "Wrong email" };
      res.status(retStatus).send(retBody);
    }
  });
};

// logout
var logout = (req, res) => {
  var email = req.body.email;
  var queryStringEmail = 'q={"email":"' + email + '"}&';
  var  client = request.createClient(config.mlabHost);
  client.get(`${config.baseURL}/users?${queryStringEmail}&apiKey=${config.apiKey}`, (err, resMlab , body) => {
    var respuesta = body[0];
    if (respuesta != undefined) {
      var session = {"logged":true};
      var logout = '{"$unset":' + JSON.stringify(session) + '}';
      client.put(`${config.baseURL}/users?q={"id": ${respuesta.id}}&apiKey=${config.apiKey}`, JSON.parse(logout), (errP, resMlabP, bodyP) => {
        res.status(200).send(body[0]);
      });
    } else {
      res.send({"msg": "Logout error"});
    }
  });
};

// GET users
var getUsers = (req, res) => {
  var client = request.createClient(config.mlabHost);
  var queryString = 'f={"_id":0}';
  client.get(`${config.baseURL}/users?${queryString}&apiKey=${config.apiKey}`, (err, resMlab, body) => {
      var response = {};
      if(err) {
          response = {
            "msg" : "Internal server error"
          }
          res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          response = {
            "msg" : "User not found"
          }
          res.status(404);
        }
      }
      res.send(response);
    });
};

// GET users con id
var getUserByID = (req, res) => {
  var id = req.params.userId;
  var client = request.createClient(config.mlabHost);
  var queryString = 'q={"id":' + id + '}&';
  var queryStrField = 'f={"_id":0}&';
  client.get(`${config.baseURL}/users?${queryString}&${queryStrField}&apiKey=${config.apiKey}`, (err, resMlab, body) => {
    var response = {};
    if(err) {
      response = {
        "msg" : "Internal server error"
      }
      res.status(500);
    } else {
      if(body.length > 0) {
        response = body;
      } else {
        response = {
          "msg" : "User not found"
        }
        res.status(404);
      }
    }
    res.send(response);
  });
};


// POST users
var postUser = (req, res) => {
  let id = req.params.userId;
  let client = request.createClient(config.mlabHost);
  let queryStrField = 'f={"_id":0}&';
  client.get(`${config.baseURL}/users?${queryStrField}&apiKey=${config.apiKey}`, (err, resMlab, body) => {
    let maxId = body.reduce((max, current) => current.id > max ? current.id : max, 0);
    let newId = maxId + 1;
    let newUser = {
      "id" : newId,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    };
    client.post(`${config.baseURL}/users?apiKey=${config.apiKey}`, newUser, (err, resMlab, body) => {
      res.send(body);
    });
  });
};

// PUT users
var putUser = (req,res) => {
  let id = req.params.userId;
  var client = request.createClient(config.mlabHost);
  let newUser = {
    id: parseInt(id),
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password
  };
  var queryString = 'q={"id":' + id + '}';
  client.put(`${config.baseURL}/users?${queryString}&apiKey=${config.apiKey}`, newUser, (err, resMlab, body) => {
    res.status(resMlab.statusCode).send(body);
  });
};

// DELETE users
var deleteUser = (req,res) => {
  let id = req.params.userId;
  var client = request.createClient(config.mlabHost);
  var queryString = 'q={"id":' + id + '}';
  let queryStrField = 'f={"_id":1}';
  client.get(`${config.baseURL}/users?${queryString}&${queryStrField}&apiKey=${config.apiKey}`, (err, resMlab, body) => {
    if (body.length > 0) {
      let mongoId = body[0]._id.$oid;
      newUser = {
        id: id,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password
      };
      client.delete(`${config.baseURL}/users/${mongoId}?&apiKey=${config.apiKey}`, (err, resMlab, body) => {
        res.status(resMlab.statusCode).send(body);
      });
    } else {
      res.status(404).send({ msg: "User not found" })
    }
  });

};

module.exports.login = login;
module.exports.logout = logout;
module.exports.getUsers = getUsers;
module.exports.getUserByID = getUserByID;
module.exports.postUser = postUser;
module.exports.putUser = putUser;
module.exports.deleteUser = deleteUser;
