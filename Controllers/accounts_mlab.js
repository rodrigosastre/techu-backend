var request = require('request-json');
var config = require('../config/config.js');

// GET accounts
var getUserAccounts = (req, res) => {
  var client = request.createClient(config.mlabHost);
  var queryStrField = 'f={"_id":0}';
  let user_email = req.query.user_email;
  if (user_email) {
    var userQueryString = `q={"email":${user_email}}`;
    client.get(`${config.baseURL}/users?${queryStrField}&${userQueryString}&apiKey=${config.apiKey}`, (err, resMlab, body) => {
      var response = {};
      if(err) {
          response = {
            "msg" : "Internal server error"
          }
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          var accountsQueryString = `q={"user_id":${body[0].id}}`;
          client.get(`${config.baseURL}/accounts?${queryStrField}&${accountsQueryString}&apiKey=${config.apiKey}`, (errAcc, resMlabAcc, bodyAcc) => {
            res.status(resMlabAcc.statusCode).send(bodyAcc);
          });
        } else {
          response = {
            "msg" : "User not found"
          }
          res.status(404);
          res.send(response);
        }
      }
    });
  } else {
    response = {
      "msg" : "Bad request"
    }
    res.status(400);
    res.send(response);
  }
};

module.exports.getUserAccounts = getUserAccounts;
