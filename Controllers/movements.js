var request = require('request-json');
var config = require('../config/config.js');

// GET movements
var getAccountMovements = (req, res) => {
  var client = request.createClient(config.mlabHost);
  var queryStrField = 'f={"_id":0}';
  let account = req.query.account;
  if (account) {
    var movementsQueryString = `q={"account":${account}}`;
    client.get(`${config.baseURL}/movements?${queryStrField}&${movementsQueryString}&apiKey=${config.apiKey}`, (err, resMlab, body) => {
      var response = {};
      if(err) {
          response = {
            "msg" : "Internal server error"
          }
          res.status(500);
          res.send(response);
      } else {
        if (body.length > 0) {
          body.sort(function(a,b) {
            aDate = a.date.split('/');
            bDate = b.date.split('/');
            if (parseInt(aDate[2]) > parseInt(bDate[2])) {
              return 1;
            }
            if (parseInt(aDate[2]) < parseInt(bDate[2])) {
              return -1;
            }
            if (parseInt(aDate[0]) > parseInt(bDate[0])) {
              return 1;
            }
            if (parseInt(aDate[0]) < parseInt(bDate[0])) {
              return -1;
            }
            if (parseInt(aDate[1]) > parseInt(bDate[1])) {
              return 1;
            }
            if (parseInt(aDate[1]) < parseInt(bDate[1])) {
              return -1;
            }
            return 0;
          });
          res.status(resMlab.statusCode).send(body);
        } else {
          response = {
            "msg" : "Movement not found"
          }
          res.status(404);
          res.send(response);
        }
      }
    });
  } else {
    response = {
      "msg" : "Bad request"
    }
    res.status(400);
    res.send(response);
  }
};

module.exports.getAccountMovements = getAccountMovements;
