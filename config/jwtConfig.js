var app_config = require('./config.js');
var jwt = require('express-jwt');

var config = jwt({ secret: app_config.jwtSecret });

module.exports.config = config;
