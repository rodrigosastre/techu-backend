// API
const URI = '/api-uruguay-mlab/v1';
const port = process.env.ML_PORT || 3001;

// MLab
const mlabHost = 'https://api.mlab.com';
const baseURL = '/api/1/databases/techuuruguay/collections';
const apiKey = 'puoqllGnzqgwV0ZXLfypF-WPEcOKcQaM';
const jwtSecret = 'jw753cr37';
const tokenExpiration = '1h';

module.exports.URI = URI;
module.exports.port = port;
module.exports.mlabHost = mlabHost;
module.exports.baseURL = baseURL;
module.exports.apiKey = apiKey;
module.exports.jwtSecret = jwtSecret;
module.exports.tokenExpiration = tokenExpiration;
